<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {

    Route::post('/pre_register', 'Api\Auth\RegisterController@pre_register');
    Route::post('/activate', 'Api\Auth\RegisterController@activate');
    Route::post('/register', 'Api\Auth\RegisterController@register');
    Route::post('/login', 'Api\Auth\LoginController@login');


    Route::middleware(['auth:api'])->group(function () {
        Route::post('/logout', 'Api\Auth\LoginController@logout');
        Route::post('/resend', 'Api\Auth\RegisterController@resend');

    });
});