<!DOCTYPE html>
<html>
<head>
    <title>Activation Email</title>
</head>

<body>
<h2>Welcome to the site {{$user['username']}}</h2>
<br/>
Your registered email-id is {{$user['email']}}
<br/>
Your one time password is {{$user['otp']}}
<br/>
</body>

</html>