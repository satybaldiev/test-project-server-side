<?php

namespace App\Http\Controllers\Api\Auth;

use App\Error;
use App\Mail\OtpMail;
use App\OtpModel;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'username'=>'required|max:55',
            'email'=>'email|required|unique:users',
            'password'=>'required'
        ]);

        if ($validator->fails()) {
            $response['status'] = '422';
            $response['message'] = $validator->errors()->toJson();
            return response()->json($response, 202);
        }


        $user =  User::create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'email_verified_at' => Carbon::now()
        ]);
        $token = $user->setNewAccessToken();
        $user->token = $token;
        $user->token_expire_date = $user->api_timestamp;

        $response['status']  = 0;
        $response['message'] = '';
        $response['data']    = $user;

        return response()->json($response, 200);
    }

    public function pre_register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'=>'required|unique:users',
            'email'=>'email|required|unique:users',
        ]);

        if ($validator->fails()) {
            $response['status'] = '422';
            $response['message'] = $validator->errors()->toJson();
            return response()->json($response, 202);
        }



        $otp = OtpModel::updateOrCreate(
            [
                'email' => $request->email
            ],
            [
                //'otp' => mt_rand(100000,999999),
                'otp' => 123456,
                'expire_date' => Carbon::now()->addHours(1)
            ]
        );

        $otp->username = $request->username;

        Mail::to($otp->email)->send(new OtpMail($otp));

        $response['status']  = 0;
        $response['message'] = '';
        $response['data']    = [];


        return response()->json($response, 200);
    }


    public function activate(Request $request)
    {

        $otp = OtpModel::where('email',$request->email)->first();;

        if ($otp != null && $otp->otp == $request->otp &&  Carbon::now() <= $otp->expire_date)
        {
            $response['status']  = 0;
            $response['message'] = '';
            $response['data']    = [];
            $otp->delete();
            return response()->json($response, 200);
        }
        else
            {
                $response['status'] = '422';
                $response['message'] = "Otp expired or invalid";
                return response()->json($response, 202);
            }

    }


    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            return response()->json('User already have verified email!', 422);
        }
        $request->user()->sendEmailVerificationNotification();
        return response()->json('The notification has been resubmitted');
    }

}
