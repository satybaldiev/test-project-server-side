<?php

namespace App\Http\Controllers\Api\Auth;

use App\Error;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //
    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if(!auth()->attempt($loginData)) {
            return Error::response('incorrect_login');
        }
        $user = auth()->user();

        $user->token = $user->setNewAccessToken();
        $user->token_expire_date = $user->api_timestamp;

        $response['status']  = 0;
        $response['message'] = '';
        $response['data']    = $user;
        return response()->json($response, 200);
    }

    public function logout() {
        $user = Auth::user();
        $user->access_token = null;
        $user->token_expire_date = null;
        $user->save();
        $response['status']  = 0;
        $response['message'] = '';
        return $response;
    }
}
