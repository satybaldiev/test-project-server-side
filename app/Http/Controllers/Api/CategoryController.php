<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function getCategories(Request $request)
    {
        $id = auth()->user()->id;
        $categories = DB::table('categories')->where('user_id', $id)->get();
        return response(['categories' => $categories]);
    }


    public function save(Request $request)
    {
        $id = auth()->user()->id;

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'icon' => 'required',
            'color' => 'required',
            'default_weight' => 'required',
            'order' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['message' => $validator->messages()],422);
        }

        $category = new Category();
        $category->user_id = $id;
        $category->fill($request->all());
        $category->save();

        return response(['category' => $category],201);


    }

    public function get($id)
    {
        $category = Category::where('id', $id)->first();
        if ($category == null) {
            return response(['message' => 'Not found'], 404);

        }
        return response(['category' => $category, 'message' => 'Success'], 200);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'icon' => 'required',
            'color' => 'required',
            'default_weight' => 'required',
            'order' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['message' => $validator->messages()],422);
        }

        $category = Category::find($id);
        if ($category == null) {
            return response(['message' => 'Not found'], 404);
        }
        $category->fill($request->all());
        $category->save();

        return response(['category' => $category, 'message' => 'Success'], 200);
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        if ($category != null) {
            $category->delete();
            return response(['message' => 'Success'], 200);
        } else {
            return response(['message' => 'Not found'], 404);
        }
    }
}
