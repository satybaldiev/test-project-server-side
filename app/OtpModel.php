<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtpModel extends Model
{
    protected $fillable=["email","otp","expire_date"];

}
