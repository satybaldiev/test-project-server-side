<?php

namespace App;

class Error {
    protected static $errors = [
        'incorrect_api_version' => [
            'code'    => 1100,
            'message' => [
                'en' => 'Incorrect API version',
                'ru' => 'Версия API не соответствует',
            ],
        ],
        'incorrect_old_password' => [
            'code'    => 1150,
            'message' => [
                'en' => 'Incorrect old password',
                'ru' => 'Неверный старый пароль',
            ],
        ],
        'text_is_too_short' => [
            'code'    => 1160,
            'message' => [
                'en' => 'Text is too short',
                'ru' => 'Текст слишком короткий',
            ],
        ],
        'parameter_required' => [
            'code'    => 1200,
            'message' => [
                'en' => 'Some parameters are missing',
                'ru' => 'Один из параметров отсутствует',
            ],
        ],
        'parameter_empty' => [
            'code'    => 1300,
            'message' => [
                'en' => 'Some parameters are empty',
                'ru' => 'Значение один из параметров пустой',
            ],
        ],
        'token_time_expired' => [
            'code'    => 1400,
            'message' => [
                'en' => 'Token is expired',
                'ru' => 'Время токена истек',
            ],
        ],
        'incorrect_login' => [
            'code'    => 1500,
            'message' => [
                'en' => 'Entered login is incorrect',
                'ru' => 'Введенные данные неверные',
            ],
        ],
        'wrong_variable_is_received' => [
            'code'    => 1550,
            'message' => [
                'en' => 'Wrong variable is received',
                'ru' => 'Получена неправильная переменная',
            ],
        ],
        'wrong_image_type' => [
            'code'    => 1560,
            'message' => [
                'en' => 'Wrong image type',
                'ru' => 'Неправильный тип изображения',
            ],
        ],
        'file_size_too_big' => [
            'code'    => 1570,
            'message' => [
                'en' => 'File size is too big',
                'ru' => 'Размер файла слишком большой',
            ],
        ],
        'item_exists' => [
            'code'    => 1600,
            'message' => [
                'en' => 'Item already exists',
                'ru' => 'Данные уже существуют',
            ],
        ],
        'item_not_found' => [
            'code'    => 1700,
            'message' => [
                'en' => 'Item is not found',
                'ru' => 'Данные не существуют',
            ],
        ],
        'user_not_found' => [
            'code'    => 1800,
            'message' => [
                'en' => 'User is not found',
                'ru' => 'Пользователь не существуют',
            ],
        ],
        'incorrect_device_type' => [
            'code'    => 1900,
            'message' => [
                'en' => 'Incorrect device type',
                'ru' => 'Неверный вид устройства',
            ],
        ],
        'incorrect_user_role' => [
            'code'    => 1950,
            'message' => [
                'en' => 'Incorrect user role',
                'ru' => 'Неверная привилегия пользователя',
            ],
        ],
        'user_access_denied' => [
            'code'    => 2000,
            'message' => [
                'en' => 'User access denied',
                'ru' => 'Отказано в доступе к пользователю',
            ],
        ],
    ];

    public static function response($error) {
        $response['status']  = self::$errors[$error]['code'];
        $response['message'] = (string) self::$errors[$error]['message']["ru"];
        $response['data']    = [];

        return $response;
    }
}
